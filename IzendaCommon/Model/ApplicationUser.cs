﻿namespace IzendaCommon.Model
{
    /// <summary>
    /// New user entity - Application User
    /// </summary>
    public class ApplicationUser
    {
        #region Properties
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public int? TenantId { get; set; } 
        #endregion
    }
}
