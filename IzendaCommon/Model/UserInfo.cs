﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IzendaCommon.Model
{
    public class UserInfo
    {
        #region Variables
        private IList<RoleInfo> _roles;
        #endregion

        #region Properties
        [Display(Name = "User ID")]
        public string UserName { get; set; }

        [Display(Name = "Tenant")]
        public string TenantUniqueName { get; set; }

        public string Password { get; set; }

        public IList<RoleInfo> Roles
        {
            get { return _roles ?? (_roles = new List<RoleInfo>()); }
            set { _roles = value; }
        }
        #endregion
    }
}
