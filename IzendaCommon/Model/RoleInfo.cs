﻿using System;

namespace IzendaCommon.Model
{
    public class RoleInfo
    {
        #region Properties
        public Guid Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
