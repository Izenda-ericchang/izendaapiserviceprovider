﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IzendaMVCCore.Controllers
{
    [Route("api/tenant")]
    public class TenantController : BaseController
    {
        #region Variables
        private readonly string _defaultConnectionString;
        #endregion

        #region CTOR
        public TenantController(IConfiguration configuration) => _defaultConnectionString = configuration.GetConnectionString("DefaultConnection");
        #endregion

        #region Methods
        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("CreateTenant")]
        public async Task<JsonResult> CreateTenant(string tenantID, string tenantName)
        {
            if (string.IsNullOrEmpty(tenantID) || string.IsNullOrEmpty(tenantName))
                return AddJsonResult(false);
            else
            {
                var adminToken = IzendaCommon.IzendaBoundary.IzendaTokenAuthorization.GetIzendaAdminToken();
                var isTenantExist = IzendaCommon.SampleUserDB.SampleUserDbHelper.GetTenantByName(tenantID, _defaultConnectionString);

                if (isTenantExist == null)
                {
                    // create a new Izenda tenant in configuration db
                    var success = await IzendaCommon.IzendaBoundary.IzendaEndPoint.CreateTenant(tenantName, tenantID, adminToken);

                    if (success)
                    {
                        // create a new tenant in sample user db
                        await IzendaCommon.SampleUserDB.SampleUserDbHelper.SaveTenantAsync(new IzendaCommon.Model.Tenant() { Name = tenantID }, _defaultConnectionString);
                        return AddJsonResult(true);
                    }
                    else
                        return AddJsonResult(false);
                }
                else
                    return AddJsonResult(false);
            }
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("GetTenantList")]
        public string GetTenantList()
        {
            var tenantList = IzendaCommon.SampleUserDB.SampleUserDbHelper.GetAllTenants(_defaultConnectionString);

            var list = new List<string>();
            list.Add("Select Tenant");

            foreach (var tenant in tenantList)
            {
                list.Add(tenant.Name);
            }

            var result = JsonConvert.SerializeObject(list);

            return result;
        }
        #endregion
    }
}
