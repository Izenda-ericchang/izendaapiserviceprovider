﻿using IzendaCommon.IzendaBoundary;
using IzendaCommon.Model;
using Microsoft.AspNetCore.Mvc;

namespace IzendaMVCCore.Controllers.ApiControllers
{
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        #region Methods
        [HttpGet]
        [Route("validateIzendaAuthToken")]
        public UserInfo ValidateIzendaAuthToken(string access_token)
        {
            var userInfo = IzendaTokenAuthorization.GetUserInfo(access_token);
            return userInfo;
        }

        [HttpGet]
        [Route("GetIzendaAccessToken")]
        public ActionResult<string> GetIzendaAccessToken(string message)
        {
            var userInfo = IzendaTokenAuthorization.DecryptIzendaAuthenticationMessage(message);
            var token = IzendaTokenAuthorization.GetToken(userInfo);

            return Ok(new { Token = token });
        }
        #endregion
    }
}
