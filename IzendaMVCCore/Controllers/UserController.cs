﻿using IzendaCommon.IzendaBoundary;
using IzendaCommon.Model;
using IzendaCommon.SampleUserDB;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IzendaMVCCore.Controllers
{
    [Route("api/user")]
    public class UserController : BaseController
    {
        #region Variables
        private readonly string _defaultConnectionString;
        #endregion

        #region CTOR
        public UserController(IConfiguration configuration) { _defaultConnectionString = configuration.GetConnectionString("DefaultConnection"); }
        #endregion

        #region Methods
        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("GenerateToken")]
        public async Task<JsonResult> GenerateToken(string tenant, string email, string password)
        {
            var loginAttempt = false;

            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            bool.TryParse(configuration.GetValue<string>("AppSettings:Settings:useADlogin"), out bool useADlogin);

            if (!string.IsNullOrEmpty(tenant) && useADlogin) // if tenant is null, then assume that it is system level login. Go to the ValidateLogin which is used for regular login process first
            {
                bool success = ValidateActiveDirectoryLogin(tenant);
               
                if (success)
                    loginAttempt = await AuthenticateUser(email, password, tenant);
            }
            else
                loginAttempt = await AuthenticateUser(email, password, tenant);

            if (!loginAttempt)
                return null;

            // Login success, create UserInfo to GetToken
            var user = new UserInfo
            {
                UserName = email,
                TenantUniqueName = tenant,
                Password = password
            };

            // get token from constructed user information
            var token = IzendaTokenAuthorization.GetToken(user);

            return Json(new { token });
        }

        private async Task<bool> AuthenticateUser(string userName, string password, string tenantName)
        {
            var users = SampleUserDbHelper.GetUserList(userName, _defaultConnectionString);

            // Can't find user. Login failed.
            if (!users.Any())
                return false;

            // find specific user by tenant
            var currentUser = users.FirstOrDefault(u => u.TenantUniqueName == tenantName);

            // no matching user + tenant found
            if (currentUser == null)
                return false;

            // check if password matches
            var userPasswordFromDB = string.Empty;

            try
            {
                userPasswordFromDB = IzendaTokenAuthorization.GetPassword(currentUser.Password);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                // failed to verify hashed password
                return false;
            }

            if (password != null && password.Equals(userPasswordFromDB))
            {
                // retrieve role list assigned to user
                var roleList = SampleUserDbHelper.GetUserRoles(userName, _defaultConnectionString).Result;

                var claims = new List<Claim>();

                var nameClaim = new Claim("userName", userName);
                claims.Add(nameClaim);

                if (tenantName != null)
                {
                    var tenantClaim = new Claim("tenantName", tenantName);
                    claims.Add(tenantClaim);
                }

                if (roleList.Any(r => r == "Admin"))
                {
                    var roleClaim = new Claim("IsAdmin", "True");
                    claims.Add(roleClaim);
                }
                else
                {
                    var roleClaim = new Claim("IsAdmin", "False");
                    claims.Add(roleClaim);
                }

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties
                {
                    #region Properties can be set for Cookie based authentication
                    //AllowRefresh = <bool>,
                    // Refreshing the authentication session should be allowed.

                    //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                    // The time at which the authentication ticket expires. A 
                    // value set here overrides the ExpireTimeSpan option of 
                    // CookieAuthenticationOptions set with AddCookie.

                    //IsPersistent = true,
                    // Whether the authentication session is persisted across 
                    // multiple requests. When used with cookies, controls
                    // whether the cookie's lifetime is absolute (matching the
                    // lifetime of the authentication ticket) or session-based.

                    //IssuedUtc = <DateTimeOffset>,
                    // The time at which the authentication ticket was issued.

                    //RedirectUri = <string>
                    // The full path or absolute URI to be used as an http 
                    // redirect response value.
                    #endregion
                };

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Login with Active Directory information.
        /// Please refer to the following link to get more information on Active Directory 
        /// https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/get-started/virtual-dc/active-directory-domain-services-overview
        /// </summary>
        private bool ValidateActiveDirectoryLogin(string tenant)
        {
            var userName = Environment.UserName;
            var userDomainName = Environment.UserDomainName;
            var authenticationType = ContextType.Domain;

            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(userDomainName))
            {
                using (var context = new PrincipalContext(authenticationType, Environment.UserDomainName))
                {
                    var userPrincipal = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

                    if (userPrincipal != null)
                    {
                        var email = userPrincipal.EmailAddress;
                        var users = SampleUserDbHelper.GetUserList(email, _defaultConnectionString); // get user list from DB

                        // if matches with tenant information, then authentication is successfull.
                        return users != null ? users.FirstOrDefault(u => u.TenantUniqueName == tenant) != null : false;
                    }
                }
            }

            return false;
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("CreateUser")]
        public async Task<JsonResult> CreateUser(bool isAdmin, string selectedRole, string selectedTenant, string userId, string firstName, string lastName, string password)
        {
            var izendaAdminAuthToken = IzendaTokenAuthorization.GetIzendaAdminToken();

            int? tenantId = null;

            if (selectedTenant != "Select Tenant") // tenant level
            {
                tenantId = SampleUserDbHelper.GetTenantByName(selectedTenant, _defaultConnectionString)?.Id;
                isAdmin = false;

                if (tenantId == null)
                    return AddJsonResult(false);
            }

            var users = SampleUserDbHelper.GetUserList(userId, _defaultConnectionString);

            // invalid user input - duplicate
            if (users.Any())
                return AddJsonResult(false);

            var assignedRole = !string.IsNullOrEmpty(selectedRole) ? selectedRole : "Employee"; // set default role if required

            // save user into sample user DB
            await SampleUserDbHelper.SaveUserAsync(userId, userId, assignedRole, tenantId, password, _defaultConnectionString);

            var success = await IzendaEndPoint.CreateIzendaUser(
                selectedTenant,
                userId,
                lastName,
                firstName,
                isAdmin,
                assignedRole, 
                izendaAdminAuthToken);

            if (success)
                return AddJsonResult(true);
            else
                return AddJsonResult(false);
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("GetRoleList")]
        public async Task<string> GetRoleList(string selectedTenant)
        {
            var selectList = new List<string>();
            var adminToken = IzendaTokenAuthorization.GetIzendaAdminToken();

            var izendaTenant = await SampleUserDbHelper.GetIzendaTenantByTenantId(selectedTenant, adminToken);
            var roleDetailsByTenant =  await IzendaEndPoint.GetAllIzendaRoleByTenant(izendaTenant?.Id ?? null, adminToken);

            foreach (var roleDetail in roleDetailsByTenant)
            {
                selectList.Add(roleDetail.Name);
            }
            var result = JsonConvert.SerializeObject(selectList);

            return result;
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("GetCurrentUserInfo")]
        public string GetCurrentUserInfo(string token)
        {
            var userInfo = IzendaTokenAuthorization.GetUserInfo(token);
            var result = JsonConvert.SerializeObject(userInfo.UserName);

            return result;
        }
        #endregion
    }
}
